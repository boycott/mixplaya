'use strict';
const fs = require('fs')
const path = require('path')

function mixPlaya (files) {

  function createPlaylists (box, contents = {}) {

    function create (name, link, volume) {
      const panner = ctx.createPanner()

      panner.panningModel = 'HRTF'
      panner.connect(ctx.destination)

      contents[name] = {
        files: [],
        playitems: [],
        panner,
        link,
        volume,
        centre : {
          x: 25,
          y: 25
        }
      }

      resetVolume(name)
    }

    function add (name, audio) {
      contents[name].files.push(audio)
    }

    function get (name) {
      return !!contents[name]
    }

    function setPanner (name, coords) {
      const item = contents[name]

      if (coords.x) item.panner.positionX.value = coords.x
      if (coords.y) item.panner.positionY.value = coords.y
      if (coords.z) item.panner.positionZ.value = coords.z
    }

    function setPosition (name, x, y) {
      const item = contents[name]

      if (typeof item === 'undefined' || item.files.length === 0) {
        setTimeout(() => setPosition(name, x, y), 100)
        return
      }

      if (!item.source) {
        contents[name].playitems = [...item.files]
        play(name)
      }

      item.link.style.left = (x - item.centre.x) + 'px'
      item.link.style.top  = (y - item.centre.y) + 'px'

      setPanner(name, {
        x: (((x - box.left) / box.width) * 2) - 1,
        y: (((y - box.top) / box.height) * 2) - 1
      })
    }

    function setVolume (name) {
      const amplifier = 5

      const z = (Math.log(contents[name].volume.value / 10000) * amplifier) - amplifier 

      setPanner(name, {z})
    }

    function resetVolume (name) {
      const item = contents[name]

      item.volume.value = item.volume.defaultValue
      setVolume(name)
    }

    function play (name) {
      const item = contents[name]

      const source = ctx.createBufferSource()
      const idx = Math.floor(Math.random() * item.playitems.length)
      const remove = Math.floor(Math.random() * item.playitems.length) || 1
      const file = contents[name].playitems.splice(idx, remove)[0]

      source.buffer = file

      source.addEventListener('ended', () => {
        if (contents[name].playitems.length) play(name)
        else stop(name)
      })

      source.connect(item.panner)

      source.start(0)

      contents[name].source = source
    }

    function stop (name) {
      const item = contents[name]

      if (item.source) {
        item.source.stop()
        contents[name].source = null
      }

      item.link.style.left = null 
      item.link.style.top  = null 

      resetVolume(name)
    }

    return {create, add, get, setPosition, setVolume, stop}
  }

  function setDropped (e) {
    e.preventDefault()
    e.stopPropagation()

    const name = e.dataTransfer.getData('Text')

    if (name) {
      if (/(svg|g|line)/.test(e.target.nodeName)) {
        playlists.setPosition(name, e.clientX, e.clientY)
      } else {
        playlists.stop(e.dataTransfer.getData('Text'))
      }
    } else {
      Object.keys(e.dataTransfer.files).map(k => {
        const basedir = e.dataTransfer.files[k]
        const subdirs = fs.readdirSync(basedir.path).filter(p => fs.lstatSync(path.join(basedir.path, p)).isDirectory())

        subdirs.map(subdir => {
          const files = fs.readdirSync(path.join(basedir.path, subdir)).filter(filename => /\.mp3$/.test(filename)).map(filename => path.join(basedir.path, subdir, filename))

          createPlaylist(subdir, files)
        })
      })
    }
  }

  function setupApp () {
    document.body.addEventListener('dragover', (e) => {
      e.preventDefault()
      e.stopPropagation()

      e.dataTransfer.dropEffect = 'move'

      return false
    })

    document.body.addEventListener('drop', setDropped)
  }

  function loadFile (name, filename) {
    const buffer = fs.readFileSync(filename)

    const ab = new Uint8Array(buffer).buffer

    ctx.decodeAudioData(ab, (audio) => playlists.add(name, audio))
  }

  function createPlaylist (name, files) {
    const listItem = document.importNode(template.content, true)
    const link = listItem.querySelector('a')
    const volume = listItem.querySelector('input')

    const dropMsg = document.body.querySelector('p')
    if (dropMsg) dropMsg.parentNode.removeChild(dropMsg)

    listItem.querySelector('li').className = name

    link.innerHTML = name[0].toUpperCase()

    playlists.create(name, link, volume)

    files.map(file => loadFile(name, file))

    link.addEventListener('dragstart', (e) => {
      e.dataTransfer.effectAllowed = 'move'
      e.dataTransfer.setData('Text', name)
    })

    volume.addEventListener('change', (e) => {
      if (playlists.get(name)) playlists.setVolume(name)
    })

    listItem.querySelector('span').addEventListener('click', (e) => {
      if (playlists.get(name)) playlists.stop(name)
    })

    nav.appendChild(listItem)
  }

  function setupSVG () {
    const svg = document.body.querySelector('svg')

    svg.addEventListener('drop', setDropped)

    return svg.getBoundingClientRect()
  }
  const nav = document.body.querySelector('ul')
  const template = document.getElementById('playlist')
  const ctx = new window.AudioContext()
  const playlists = createPlaylists(setupSVG())

  setupApp()
}

window.addEventListener('DOMContentLoaded', () => mixPlaya() )
